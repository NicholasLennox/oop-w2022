﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_W2022
{
    public interface IGearable
    {
        public void GearUp();
        public void GearDown();
        public double GetGear();
    }
}
