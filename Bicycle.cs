﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_W2022
{
    public abstract class Bicycle // sealed = cannot inherit from class
    {
        // State
        public string Name { get; set; }
        public int Capacity { get; set; }

        // Constructors
        protected Bicycle()
        {
        }

        protected Bicycle(string name, int capacity)
        {
            Name = name;
            Capacity = capacity;
        }

        // Behaviours
        public virtual string Hoot() // Virtual = optional override
        {
            return "Tring tring";
        }

        public abstract void Ride(); // Abstract forced override
    }
}
