﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_W2022
{
    public class Engine
    {
        public int Capacity { get; set; }
        public double FuelLevel { get; set; } = 0;
    }
}
