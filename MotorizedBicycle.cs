﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_W2022
{
    public class MotorizedBicycle : Bicycle, IGearable
    {
        // State
        private Engine _engine = new Engine();
        private int _gear = 1;

        // Constructor
        public MotorizedBicycle(string name, int capacity, Engine engine) : base(name, capacity)
        {
            _engine = engine;
        }

        // Behaviour
        public override void Ride()
        {
            Console.WriteLine("Broom broom");
            _engine.FuelLevel -= 10;
        }

        public void GearUp()
        {
            _gear++;
        }

        public void GearDown()
        {
            _gear--;
        }

        public double GetGear()
        {
            return _gear;
        }
    }
}
