﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_W2022
{
    public class Bmx : Bicycle
    {
        // Extended state
        public string Pegs { get; set; } = "Back";

        // Constructor
        public Bmx(string name, int capacity, string pegs) : base(name, capacity)
        {
            Pegs = pegs;
        }

        // Behaviour
        public void ShowPegs()
        {
            Console.WriteLine($"There are {Pegs}");
        }

        public override void Ride() // Abstract
        {
            Console.WriteLine("Doing a massive flip!");
        }

        public override string Hoot()  // Virtual
        {
            return "Air horn noises";
        }
    }
}
